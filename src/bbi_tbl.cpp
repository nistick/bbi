#pragma once

#include "bbi.hpp"
#include "bbi_prot.hpp"

vector<SymTbl> Gtable;
vector<SymTbl> Ltable;
int startLtable;

/*
 * Data feeding to Symbol Table by name and type
 * */
int enter(SymTbl& tb, SymKind kind){
    int n, mem_size;
    bool isLocal = is_localName(tb.name, kind);
    extern int localAdrs;
    extern Mymemory Dmem;
    
    // check
    mem_size = tb.arylen;
    if (mem_size==0) mem_size = 1;
    if(kind !=varId && tb.name[0] == '$')
        err_exit("cann't use doller symbol not in variable name");
    tb.nmKind = kind;
    n = -1;
    if(kind == fncId) n = searchName(tb.name, 'G');
    if(kind == paraId) n = searchName(tb.name, 'L');
    if(n != -1) err_exit("Duplicate Variable Name", tb.name);    

    // setting address
    if(kind == fncId) tb.adrs = get_lineNo();
    else{
        if(isLocal){
            tb.ardrs = localAdrs;
            localAdrs += mem_size; 
        } else {
            tb.adrs = Dmem.size();
            Dmem.resize(Dmem.size() + mem_size);
        }
    }

    // register
    if(isLocal){n = Ltable.size(); Ltable.push_back(tb);}
    else {n = Gtable.size(); Gtable.push_back(tb);}
    return n;
}
void set_startLtable()
{
    startLtable = Ltable.size();
}

bool is_localName(const string& name, SymKind kind)
{
    if(kind == paraId) return true;
    if(kind == varId)
    {
        if(is_localScope() && name[0]!='$' return true; else return false;)
    }
    return false;
}

/*
 * True if name is in Gtable or Ltable
 * */
int searchName(const string& s, int mode)
{
    int n;
    switch(mode)
    {
        case 'G':
            for(n=0; n<(int)Gtable.size(); n++)
            {
                if(Gtable[i].name == s) return true;
            }
            break;
        case 'L':
            for(n=startLtable; n(int)Ltable.size(); n++)
            {
                if(Ltable[n].name == s) return true;
            }
            break;
        case 'F':
            n = searchName(s, 'G');
            if(n != -1 && Gtable[n].nmKind == fncId) return n;
            break;
        case 'V':
            if(searchName(s, 'F') != -1) err_exit("Duplicate with Function Name", s);
            if(s[0] == '$') return searchName(s, 'G');
            if(is_localScope()) return searchName(s, 'L');
            else                return searchName(s, 'G')

    }
    return -1;
}
vector<SymTbl>::iterator tableP(const CodeSet& cd)
{
    if(cd.kind == Lvar) return Ltable.begin() + cd.symNbr;
    return Gtable.begin() + cd.symNbr;
}

